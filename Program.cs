﻿using System;
using System.Collections.Generic;

namespace seminar0606
{
    enum Food { Fish = 5, Meat = 10 }

    class Cat
    {
        public int LevelSatiety {get; private set;}

        public void EatSomething(Food food) 
        {
            this.LevelSatiety += (int)food;

            Console.WriteLine(this.LevelSatiety);
        }

        public Cat()
        {
            this.LevelSatiety = 0;
        }

    }

    struct Circle
    {
        public double Radius;
        public Circle(double r)
        {
            this.Radius = r;
        }
    }
    class Program
    {

        static void ShowChildrenInfo(string parentName, int? childrenCount) 
        {
            string generalInfo = $"У {parentName} ";

            if (childrenCount == null)
            {
                Console.WriteLine(generalInfo + "неизвестное количество детей");
            }
            else if (childrenCount == 0)
            {
                Console.WriteLine(generalInfo + "нет детей");
            }
            else
            {
                Console.WriteLine(generalInfo + $"{childrenCount} детей");
            }
        }
        static void Main(string[] args)
        {
            Cat cat = new Cat();

            cat.EatSomething(Food.Fish);
            cat.EatSomething(Food.Meat);

            ShowChildrenInfo("Василий", null);
            ShowChildrenInfo("Василий", 0);
            ShowChildrenInfo("Василий", 2);

            Random rand = new Random();
            int count = 10;
            double generalRaduis = 0;

            List<Circle> circleList = new List<Circle>();

            for (int i = 0; i < count; i++)
            {
                double radius = Convert.ToDouble(rand.Next(10, 100) / 10.0);

                generalRaduis += radius;

                Circle temp = new Circle(radius);
                circleList.Add(temp);

                Console.WriteLine($"{i}): " + temp.Radius);
            }

            Console.WriteLine($"Сумма радиусов: {generalRaduis}");

            double mediumRadius = generalRaduis / count;

            Console.WriteLine($"Средний радиус: {mediumRadius}");

            double nearestDistToMedium = generalRaduis;
            int nearestCircle = 0;

            for (int i = 0; i < count; i++)
            {
                if (Math.Abs(circleList[i].Radius - mediumRadius) < nearestDistToMedium)
                {
                    nearestDistToMedium = Math.Abs(circleList[i].Radius - mediumRadius);
                    nearestCircle = i;
                }
            }

            Console.WriteLine($"{nearestCircle} - Самая близкая окружность к среднему радиусу");

        }
    }
}
